package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
class Greeter{
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();
        Utilities utils = new Utilities();

        int userInput = keyboard.nextInt();
        System.out.println(utils.doubleMe(userInput));
    }
}